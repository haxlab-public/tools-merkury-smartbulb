# haXlab Toolkit for Merkury Smart bulb 

Please visit https://haxlab.atlassian.net/wiki/spaces/MSB/overview for the most up to date documentation.

The docs folder contains the most up to date documentation for this fixture.

The scripts folder contains scripts that are found in ~/git/tools-merkury-smartbulb
